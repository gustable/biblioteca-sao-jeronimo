# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

leitor1 = Leitor.new
leitor1.email = "email@email.com"
leitor1.password = "123456"
leitor1.send

book_list = [
	['123-456-789', 'Auto da Compadecida', 'Ariano Suassuna', 455, 'Usado', leitor1],
	['132-456-789', '1984', 'George Orwell', 560, 'Usado', leitor1],
	['142-456-799', 'Revolução dos Bichos', 'George Orwell', 455, 'Usado', leitor1],
	['124-456-789', 'Ortodoxia', 'G.K.Chesterton', 455, 'Semi-novo', leitor1]
]

book_list.each do |isbn, name, author, pages, condition, leitor|
  Book.create( isbn: isbn, name: name, author: author, pages: pages, condition: condition, leitor: leitor )
end