class AddAttachmentAvatarToLeitors < ActiveRecord::Migration
  def self.up
    change_table :leitors do |t|
      t.attachment :avatar
    end
  end

  def self.down
    remove_attachment :leitors, :avatar
  end
end
