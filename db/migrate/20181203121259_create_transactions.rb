class CreateTransactions < ActiveRecord::Migration
  def change
    create_table :transactions do |t|
      t.string :owner
      t.string :borrower

      t.timestamps null: false
    end
  end
end
