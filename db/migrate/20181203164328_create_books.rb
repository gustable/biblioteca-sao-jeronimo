class CreateBooks < ActiveRecord::Migration
  def change
    create_table :books do |t|
      t.string :isbn
      t.string :name
      t.string :author
      t.string :pages
      t.string :condition
      t.references :leitor, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
