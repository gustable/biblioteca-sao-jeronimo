class Book < ActiveRecord::Base
  belongs_to :leitor
  validates_presence_of :name, :isbn, :author, :pages, :condition

  has_attached_file :image, styles: { medium: "170x240>", thumb: "85x120>"}, default_url: "/images/:style/missing.png"
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\z/
end
