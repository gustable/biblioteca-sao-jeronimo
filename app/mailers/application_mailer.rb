class ApplicationMailer < ActionMailer::Base
  default from: "noreply@bibliotecasaojeronimo.com"
  layout 'mailer'
end
