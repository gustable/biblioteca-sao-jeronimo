class Site::Profile::DashboardController < ApplicationController
	before_action :set_book, only: [:edit]
	layout "site"
	def index
		@books = Book.where(leitor_id: current_leitor.id)
	end
	def edit
	end

	private
	def set_book
		@book = Book.find(params[:id])
	end
end
