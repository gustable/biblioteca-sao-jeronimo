class Site::HomeController < ApplicationController
  layout "site"
  def index
  	@books = Book.limit(100).order(created_at: :desc)
  end
end
