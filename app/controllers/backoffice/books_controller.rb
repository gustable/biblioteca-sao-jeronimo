class Backoffice::BooksController < BackofficeController
	before_action :set_book, only: [:edit, :update]

  def index
  	@books = Book.all
  end
  def new
  	@book = Book.new
  end
  def create
  	@book = Book.new(params_book)
  	if @book.save
  		redirect_to backoffice_books_index_path, notice: "O Livro '#{@book.name}' foi criado!"
  	else
  		render :new
  	end
  end
  def edit
  end
  def update
  	if @book.update(params_book)
  		redirect_to backoffice_books_index_path, notice: "O Livro '#{@book.name}' foi atualizado!"
  	else
  		render :edit
  	end
  end

  private
  def set_book
  	@book = Book.find(params[:id])
  end

  def params_book
  	params.require(:book).permit(:isbn, :name, :author, :pages, :condition)
  end
end
