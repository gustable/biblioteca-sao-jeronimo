namespace :utils do	
	desc "Cria admin"
	task :generate_admin => :environment do
		puts "Criando admin..."

		Admin.create!(
			email: 'admin@admin.com',
			password: '123456',
			password_confirmation: '123456'
			)
		puts "Admin criado!"
	end

	desc "Cria leitores fake"
	task :generate_leitores => :environment do
		puts "Criando leitores fake..."

		Leitor.create!(
			name: 'J.R.R.Tolkien',
			email: 'teste@teste.com',
			password: '123456',
			password_confirmation: '123456',
			avatar: File.new(Rails.root.join('public', 'images', 'avatar.jpeg'), 'r')
			)

		100.times do
			Leitor.create!(
				name: Faker::Name.name,
				email: Faker::Internet.email,
				password: '123456',
				password_confirmation: '123456',
				avatar: File.new(Rails.root.join('public', 'images', 'avatar.jpeg'), 'r')
				)
		end
		puts "Leitores criado!"
	end

	desc "Cria livros fake"
	task :generate_books => :environment do
		puts "Cadastrando Livros..."

		5.times do
			book = Faker::Book
			Book.create!(
				isbn: '000-000-0000-0',
				name: book.title,
				author: book.author,
				pages: Random.rand(50..1000),
				condition: 'usado',
				leitor: Leitor.first,
				image: File.new(Rails.root.join('public', 'images', "#{Random.rand(3)}.png"), 'r')
				)
		end

		100.times do
			book = Faker::Book
			Book.create!(
				isbn: '000-000-0000-0',
				name: book.title,
				author: book.author,
				pages: Random.rand(50..1000),
				condition: 'usado',
				leitor: Leitor.all.sample,
				image: File.new(Rails.root.join('public', 'images', "#{Random.rand(3)}.png"), 'r')
				)
		end

		puts "Livros cadastrados com sucesso!"

	end	
end